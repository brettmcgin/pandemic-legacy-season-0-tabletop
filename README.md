# Le Gamez (tabletop saves, many different games)

## Info

> Macos Save Dir

`~/Library/Tabletop\ Simulator/Saves/`

> Linux Save Dir

`~/.local/share/Tabletop\ Simulator/Saves/`

## Install Save

> MacOS

`cp 01_January.json ~/Library/Tabletop\ Simulator/Saves/01_January.json`

> Linux

`cp TS_Save_* ~/.local/share/Tabletop\ Simulator/Saves/`

